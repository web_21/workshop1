const request = require('request');
const express = require('express');
const app = express();

request('https://api.openbrewerydb.org/breweries?page=10', function (error, response, body) {
    var data = JSON.parse(body);
    app.get("/", (req, res, next) => {
        res.send(body);
    });
});

const PORT = process.env.PORT || 8080;

app.listen(PORT, () => console.log(`Server running on port ${PORT}`));